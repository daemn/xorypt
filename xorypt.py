#!/usr/bin/env python3

# Project home: https://gitlab.com/daemn/xorypt
# Copyright 2022 Daemn
# Licensed under the Apache License, Version 2.0

import sys
import os
from argparse import ArgumentParser, RawDescriptionHelpFormatter
from typing import Iterator, IO, Any
import hashlib
from getpass import getpass
from random import randint
from abc import ABC, abstractmethod

__doc__ = """
Simple encrypter and decrypter.
Read data, xor it with password hash and write result.
"""


class InputStreamEnded(Exception):
    """ The end of input stream was arrived. """
    pass


def first_not_none(*args) -> Any:
    """ Return first not None argument. """
    for arg in args:
        if arg is not None:
            return arg


class MetaSingleton(type):
    instance = None

    def __call__(cls, *args, **kwargs):
        if cls.instance is None:
            cls.instance = super(MetaSingleton, cls).__call__(*args, **kwargs)
        return cls.instance


class AppConfig(metaclass=MetaSingleton):
    """ Application configuration. """
    infile: str
    outfile: str
    password: str
    header: str

    def __init__(self) -> None:
        parser: ArgumentParser = ArgumentParser()
        parser.formatter_class = RawDescriptionHelpFormatter
        parser.description = __doc__
        parser.add_argument(
            '-p', '--password', dest='password', type=str, metavar='PASSWORD',
            help=(
                'Password for encryption. Can be defined by execution argument,'
                ' an environment variable XORYPT_PASSWORD or entered manually.'
            )
        )
        parser.add_argument(
            '-i', '--infile', dest='infile', type=str, metavar='FILE',
            help='Source FILE instead of data from stdin.'
        )
        parser.add_argument(
            '-o', '--outfile', dest='outfile', type=str, metavar='FILE',
            help='Destination FILE instead of stdout.'
        )
        parser.add_argument(
            '--add-header', dest='header', action='store_const', const='add',
            help='Add random bytes in head for hiding the size of the source.'
        )
        parser.add_argument(
            '--cut-header', dest='header', action='store_const', const='cut',
            help='Cut header with random bytes.'
        )
        args = parser.parse_args()

        self.infile = args.infile
        self.outfile = args.outfile
        self.password = first_not_none(args.password, os.getenv('XORYPT_PASSWORD', None))
        if self.password is None:
            self.password = getpass()
        self.header = args.header


class StreamReader:
    """ Read standard IO stream and return as integer numbers. """

    def __init__(self, stream: IO) -> None:
        self._stream: IO = stream

    def read(self) -> int:
        in_data: bytes = self._stream.read(1)
        if not in_data:
            raise InputStreamEnded()
        return in_data[0]


class StreamWriter:
    """ Write numbers to standard IO stream. """

    def __init__(self, stream: IO) -> None:
        self._stream: IO = stream

    def write(self, number: int) -> None:
        self._stream.write(number.to_bytes(1, byteorder='big'))


class ABCReadWriteContext(ABC):
    filename: str = None
    stream: IO = None

    def __init__(self, filename: str = None) -> None:
        if filename:
            self.filename = filename

    @abstractmethod
    def __enter__(self):
        pass

    def __exit__(self, *ignore) -> None:
        if self.filename and self.stream is not None:
            self.stream.close()


class WriteContext(ABCReadWriteContext):
    """ Write context manager. """

    def __enter__(self) -> StreamWriter:
        if self.filename:
            self.stream: IO = open(self.filename, 'wb')
        else:
            self.stream: IO = sys.stdout.buffer
        return StreamWriter(self.stream)


class ReadContext(ABCReadWriteContext):
    """ Read context manager. """

    def __enter__(self) -> StreamReader:
        if self.filename:
            self.stream: IO = open(self.filename, 'rb')
        else:
            self.stream: IO = sys.stdin.buffer
        return StreamReader(self.stream)


class XorMachine:
    """ Xor input numbers. """

    def __init__(self, password: str) -> None:
        self._hash: Iterator[int] = self._infinity_hash(password)

    @staticmethod
    def _infinity_hash(password: str) -> Iterator[int]:
        """ Return infinity determinate sequence of bytes. """
        current_hash: bytes = password.encode('utf-8')
        while True:
            current_hash: bytes = hashlib.sha512(current_hash).digest()
            for hash_num in current_hash:
                yield hash_num

    def xor(self, in_num: int) -> int:
        """ Xor input number and return result. """
        return in_num ^ next(self._hash)


def main() -> int:
    try:
        app_config: AppConfig = AppConfig()
        xor_machine: XorMachine = XorMachine(app_config.password)

        with WriteContext(filename=app_config.outfile) as output_stream:
            with ReadContext(filename=app_config.infile) as input_stream:

                if app_config.header == 'cut':
                    garbage_len: int = xor_machine.xor(input_stream.read())
                    for _ in range(garbage_len):
                        input_stream.read()

                if app_config.header == 'add':
                    garbage_len: int = randint(0, 255)
                    output_stream.write(xor_machine.xor(garbage_len))
                    for _ in range(garbage_len):
                        output_stream.write(randint(0, 255))

                while True:
                    output_stream.write(xor_machine.xor(input_stream.read()))

    except InputStreamEnded:
        pass

    except EnvironmentError as err:
        sys.stderr.write(str(err) + '\n')
        return 2

    except Exception as err:
        sys.stderr.write(str(err) + '\n')
        return 1

    return 0


if __name__ == '__main__':
    sys.exit(main())
