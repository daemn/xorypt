SUMMARY
=======

Simple encrypter and decrypter.
Read data, xor it with password hash and write result.

Read app help:
```shell
xorypt.py --help
```

Encrypt stdin. Use password from envvar:
```shell
export XORYPT_PASSWORD=123; cat a.txt | xorypt.py > a.txt.xorypt
```

Encrypt file. Add random-header. Ask password from user:
```shell
xorypt.py --infile a.txt --outfile a.txt.xorypt --add-header
```

Encrypt stdin. Remove random-header. Use password from exec key:
```shell
cat a.txt.xorypt | xorypt.py --password 123 --cut-header --outfile a.txt
```

LICENSE
=======

Read LICENSE file.
